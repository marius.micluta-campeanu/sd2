/*
    Micluta-Campeanu Marius
    Grupa 452
    > go run server.go
*/

package main

import (
    "fmt"
    "log"
    "net"
    "os"
    "time"
)

func main() {
    protocol := "tcp"
    addr := "127.0.0.1:9090"

    clientCount := 0
    allClients := make(map[net.Conn]int)
    newConnections := make(chan net.Conn)
    deadConnections := make(chan net.Conn)

    server, err := net.Listen(protocol, addr)
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }
    fmt.Println("Serverul așteaptă conexiuni...")
    go func() {
        for {
            conn, err := server.Accept()
            if err != nil {
                fmt.Println(err)
                os.Exit(1)
            }
            newConnections <- conn
        }
    }()

    for {

        select {

        case conn := <-newConnections:

            log.Printf("Client %d conectat", clientCount)

            allClients[conn] = clientCount
            clientCount += 1

            go func(conn net.Conn, clientId int) {

                fmt.Printf("Server a primit request de la clientul %d\n", clientId)
                fmt.Println("Server procesează datele")
                ts := time.Now()
                fmt.Printf("Server trimite %s către client\n", ts)
                conn.Write([]byte(ts.Format(time.RFC3339Nano) + "\n"))

                deadConnections <- conn

            }(conn, allClients[conn])


        case conn := <-deadConnections:
            log.Printf("Client %d deconectat", allClients[conn])
            delete(allClients, conn)
        }
    }
}
