package main

import (
    "bufio"
    //"encoding/binary"
    //"flag"
    "fmt"
    "net"
    //"os"
    //"regexp"
    //"strconv"
    "strings"
    "time"
)

func main() {
    protocol := "tcp"
    addr := "127.0.0.1:9090"
    t_min := 400000 * time.Nanosecond

    conn, err := net.Dial(protocol, addr)

    if err != nil {
        fmt.Println(err)
        return
    }
    fmt.Printf("Client conectat\n")

    req_time := time.Now()
    result_bytes, _ := bufio.NewReader(conn).ReadBytes('\n')
    resp_time := time.Now()

    result := strings.TrimSpace(string(result_bytes))
    server_time, err := time.Parse(time.RFC3339Nano, result)
    if err != nil {
        fmt.Println("parse error: ", err)
        return
    }

    latency := resp_time.Sub(req_time)
    latency_without_t_min := latency/2 - t_min
    client_time := server_time.Add(latency/2 - t_min)
    error := client_time.Sub(resp_time)

    fmt.Printf("Server time: %s\nLatency: %s\nLatency without t_min: %s\nClient time: %s\nSync time: %s\nSync err: %s\n",
               server_time, latency, latency_without_t_min, resp_time, client_time, error)
}
