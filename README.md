# Cristian's algorithm

## Tema
Am ales tema 1, Algoritmul lui Cristian de sincronizare a ceasurilor. Acesta funcționează corect în rețele cu latență scăzută, deoarece sincronizarea are loc doar în cazul în care timpul total al cererii către server (round-trip time sau RTT) este mic.

## Soluția propusă
Soluția propusă este alcătuită dintr-un server care oferă ora exactă și un client care dorește să își regleze ceasul după ora serverului. Ideea de bază a algoritmului lui Cristian este de a folosi RTT:
  * clientul cere timpul de la server
  * serverul procesează cererea și trimite înapoi un timestamp
  * clientul măsoară diferența de timp dintre momentul când a trimis cererea și momentul când a primit răspunsul (RTT)
  * clientul își actualizează ceasul, adăugând RTT/2 la eticheta de timp primită de la server, pentru a compensa perioada de timp parcursă de pachet prin rețea

## Detalii de implementare
Implementarea este reprezentată de două programe, unul pentru server și unul pentru client. Deoarece singurul rol al serverului este de a furniza o etichetă de timp, practic nu este nevoie de un mesaj special din partea clientului. Imediat ce un client se conectează, serverul îi scrie pe conexiunea asociată eticheta de timp. După ce clientul citește răspunsul, conexiunea se închide. În cazul în care formatul de date primit de la server nu este corespunzător, clientul afișează o eroare și se oprește. Am ales formatul RFC3339Nano pentru că oferă cea mai mare precizie și pentru faptul că furnizează data completă, spre deosebire de StampNano, care nu furnizează anul și fusul orar.

Pentru procesarea concurentă a cererilor mai multor clienți, se folosesc rutine go. De menționat că existența mai multor clienți va crește latența.

## Concluzii
Precizia poate fi îmbunătățită dacă este cunoscută latența minimă posibilă în rețea. Această valoare numerică poate fi obținută făcând mai multe cereri la server și observând latența minimă dintre acestea.

Algoritmul oferă o modalitate simplă de sincronizare a ceasurilor.
